package margitsziget;

import allapotter.Operator;

/**
 * Created by Erubo on 18/05/15.
 */
public class Tevekenyseg extends Operator {

    static final int GORKORCSOLYA = 1, FOCI = 2, KEREKPAR = 3, KOCOGAS = 4, TOLLASLABDA = 5;
    int cs;
    int tevekenyseg;

    public Tevekenyseg(int cs, int tevekenyseg) {
        this.cs = cs;
        this.tevekenyseg = tevekenyseg;
    }

    @Override
    public String toString() {
        return "Tevekenyseg{" +
                "cs=" + cs +
                ", tevekenyseg=" + tevekenyseg +
                '}';
    }
}
