package margitsziget;

import allapotter.Operator;

/**
 * Created by Erubo on 18/05/15.
 */
public class Gyerek extends Operator {

    int cs;
    int gyerekszam;

    public Gyerek(int cs, int gyerekszam) {
        this.cs = cs;
        this.gyerekszam = gyerekszam;
    }

    @Override
    public String toString() {
        return "Gyerek{" +
                "cs=" + cs +
                ", gyerekszam=" + gyerekszam +
                '}';
    }
}
