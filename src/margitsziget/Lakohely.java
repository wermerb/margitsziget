package margitsziget;

import allapotter.Operator;

/**
 * Created by Erubo on 18/05/15.
 */
public class Lakohely extends Operator {

    static final int ANYGALFOLD = 1, BELVAROS = 2, CSILLAGHEGY = 3, OBUDA = 4, UJPEST = 5;
    int cs;
    int lakohely;

    public Lakohely(int cs, int lakohely) {
        this.cs = cs;
        this.lakohely = lakohely;
    }

    @Override
    public String toString() {
        return "Lakohely{" +
                "cs=" + cs +
                ", lakohely=" + lakohely +
                '}';
    }
}
