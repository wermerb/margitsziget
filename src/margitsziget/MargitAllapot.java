package margitsziget;

import allapotter.Allapot;
import allapotter.HibasOperatorException;
import allapotter.Operator;

import java.util.HashSet;

/**
 * Created by Erubo on 18/05/15.
 */
public class MargitAllapot extends Allapot {

    static {
        operatorok = new HashSet<>();
        for (int cs = 1; cs <= 5; cs++) {
            for (int j = 1; j <= 5; j++) {
                operatorok.add(new Gyerek(cs, j));
                operatorok.add(new Lakohely(cs, j));
                operatorok.add(new Tevekenyseg(cs, j));
            }
        }
    }

    private int[][] h;

    public MargitAllapot() {
        h = new int[6][4];
    }

    private MargitAllapot(MargitAllapot szulo) {
        h = new int[6][4];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 4; j++) {
                h[i][j] = szulo.h[i][j];
            }
        }
    }

    @Override
    public boolean celAllapot() {
        return h[5][3] != 0;
    }

    @Override
    public boolean elofeltetel(Operator op) throws HibasOperatorException {
        if (op instanceof Gyerek){
            Gyerek gy = (Gyerek) op;
            int cs = gy.cs;
            int gyerekszam = gy.gyerekszam;

            return true;
        }else if (op instanceof Lakohely){
            Lakohely l = (Lakohely) op;
            int cs = l.cs;
            int lakohely = l.lakohely;

            return true;
        }else if (op instanceof Tevekenyseg){
            Tevekenyseg t = (Tevekenyseg) op;
            int cs = t.cs;
            int tevekenyseg = t.tevekenyseg;

            return true;
        }
        throw new HibasOperatorException();
    }

    @Override
    public Allapot alkalmaz(Operator op) throws HibasOperatorException {
        if (op instanceof Gyerek){
            Gyerek gy = (Gyerek) op;
            MargitAllapot uj = new MargitAllapot(this);
            uj.h[gy.cs][1] = gy.gyerekszam;
            return uj;
        }else if (op instanceof Lakohely){
            Lakohely l = (Lakohely) op;
            MargitAllapot uj = new MargitAllapot(this);
            uj.h[l.cs][2] = l.lakohely;
            return uj;
        }else if (op instanceof Tevekenyseg){
            Tevekenyseg t = (Tevekenyseg) op;
            MargitAllapot uj = new MargitAllapot(this);
            uj.h[t.cs][3] = t.tevekenyseg;
            return uj;
        }
        throw new HibasOperatorException();
    }
}
